import { Component, OnInit } from '@angular/core';

@Component({

selector: 'app-detalhes-serie',

templateUrl: './detalhes-serie.page.html',

styleUrls: ['./detalhes-serie.page.scss'],

})

export class DetalhesSeriePage implements OnInit {

detalhes = [

{

titulo: 'CSI: Miami',

subtitulo: 'Crime Scene Investigation',

capa: 'https://www.google.com.br/url?sa=i&url=https%3A%2F%2Fwallpapercave.com%2Fcsi-miami-wallpapers&psig=AOvVaw3_7GDV10AtWa7WL2a3DCwk&ust=1649371216938000&source=images&cd=vfe&ved=0CAoQjRxqFwoTCLijhofCgPcCFQAAAAAdAAAAABAD',

},

{

titulo: "Brooklyn Nine-Nine",

subtitulo: "",

capa: "https://www.google.com.br/url?sa=i&url=https%3A%2F%2Fwallpaperaccess.com%2Fbrooklyn-99&psig=AOvVaw2rLdGo10BtKmFPIWfy4_o1&ust=1649371464187000&source=images&cd=vfe&ved=0CAoQjRxqFwoTCJCWg9jBgPcCFQAAAAAdAAAAABAD",

},

{

titulo: "Criminal Minds",

subtitulo: "Unidade de Análise Comportamental",

capa: "https://www.google.com.br/url?sa=i&url=https%3A%2F%2Fwallpaperaccess.com%2Fcriminal-minds&psig=AOvVaw1sDIi9QqzFU0Ev2dZVXfRK&ust=1649371650484000&source=images&cd=vfe&ved=0CAoQjRxqFwoTCICnha7CgPcCFQAAAAAdAAAAABAD",

}

];

constructor() { }

ngOnInit() {

}

}